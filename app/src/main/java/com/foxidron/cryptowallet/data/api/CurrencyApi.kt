package com.foxidron.cryptowallet.data.api

import com.foxidron.cryptowallet.data.models.CurrencyResponse
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.serialization.json.Json
import javax.inject.Inject

/**
 * This class creates ktor HttpClient and make api calls to network.
 * In respond receiving currency, parse it from Json and return @CurrencyResponse model.
 * To scale app, creating HttpClient may be take out in independent hilt module
 */

class CurrencyApi @Inject constructor() {

    private val json = Json { ignoreUnknownKeys = true}

    suspend fun getCurrency(): CurrencyResponse {
        val client = HttpClient(Android)
        val response: HttpResponse = client.get("https://api.coindesk.com/v1/bpi/currentprice.json")
        return json.decodeFromString(CurrencyResponse.serializer(), response.readText())
    }
}