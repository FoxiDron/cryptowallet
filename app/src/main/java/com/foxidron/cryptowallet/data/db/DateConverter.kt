package com.foxidron.cryptowallet.data.db

import androidx.room.TypeConverter
import java.util.*


/**
 * Converter class to save date in database as long
 */

class DateConverter {

    @TypeConverter
    fun fromDate(time: Date): Long {
        return time.time
    }

    @TypeConverter
    fun toDate(time: Long): Date {
        return Date(time)
    }
}