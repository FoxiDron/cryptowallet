package com.foxidron.cryptowallet.data.db

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.foxidron.cryptowallet.data.models.Transaction

@Dao
interface TransactionsDao {

    @Insert
    suspend fun insertTransaction(transaction: Transaction)

    @Query("SELECT * FROM `Transaction` ORDER BY date DESC")
    fun getAllTransactions(): PagingSource<Int, Transaction>
}