package com.foxidron.cryptowallet.data.models

import kotlinx.serialization.Serializable

/**
 * Data class to deserialize data from api network requests
 */


@Serializable
data class CurrencyResponse(
    val bpi: BPI
)

@Serializable
data class BPI(
    val USD: Currency
)

@Serializable
data class Currency(
    val rate: String
)
