package com.foxidron.cryptowallet.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Transaction data class to save in database
 */

@Entity
data class Transaction(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val amount: Float,
    val outlay: Boolean,
    val category: String,
    val date: Date
)
