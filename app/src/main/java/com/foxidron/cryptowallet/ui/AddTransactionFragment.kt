package com.foxidron.cryptowallet.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.activityViewModels
import com.foxidron.cryptowallet.R
import com.foxidron.cryptowallet.data.models.Transaction
import com.foxidron.cryptowallet.databinding.FragmentAddTransactionBinding
import java.util.*

/**
 * Fragment to create new transactions
 */

class AddTransactionFragment : Fragment() {

    private lateinit var binding: FragmentAddTransactionBinding

    private val viewModel: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddTransactionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.typeAutoComplete.setAdapter(
            ArrayAdapter(
                requireContext(),
                android.R.layout.simple_dropdown_item_1line,
                listOf(
                    R.string.groceries,
                    R.string.taxi,
                    R.string.electronics,
                    R.string.restaurant,
                    R.string.other
                ).map { getString(it) }
            )
        )

        binding.btnAddTransaction.setOnClickListener {
            if (binding.amountEditText.text.isNullOrEmpty()) {
                binding.amountTextInput.error = getString(R.string.required)
                return@setOnClickListener
            } else
                binding.amountTextInput.error = null
            if (binding.typeAutoComplete.text.isNullOrEmpty()) {
                binding.typeTextInput.error = getString(R.string.required)
                return@setOnClickListener
            } else
                binding.typeTextInput.error = null
            viewModel.insertTransaction(
                Transaction(
                    amount = binding.amountEditText.text?.toString()?.toFloat() ?: 0f,
                    outlay = binding.cbOutlay.isChecked,
                    category = binding.typeAutoComplete.text.toString(),
                    date = Date()
                )
            )
            requireActivity().onBackPressed()
        }
    }
}