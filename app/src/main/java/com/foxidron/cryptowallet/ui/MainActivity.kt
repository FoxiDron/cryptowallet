package com.foxidron.cryptowallet.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.foxidron.cryptowallet.R
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * MainActivity class to host nested fragments and main viewModel
 */

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        viewModel.updateCurrency()
    }

    fun navigateToAddTransactionFragment() {
        supportFragmentManager.commit {
            replace<AddTransactionFragment>(R.id.fragment_container_view)
            addToBackStack(AddTransactionFragment::class.simpleName)
            setUpToolbar(true, R.string.add_transaction)
        }
    }

    fun setUpToolbar(showBackButton: Boolean, title: Int) {
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(showBackButton)
            it.setDisplayShowHomeEnabled(showBackButton)
            it.setTitle(title)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()
    }
}