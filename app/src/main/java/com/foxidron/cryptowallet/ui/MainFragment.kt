package com.foxidron.cryptowallet.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.foxidron.cryptowallet.R
import com.foxidron.cryptowallet.databinding.FragmentMainBinding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


/**
 * Fragment to show current currency BTC to USD and transactions
 */

class MainFragment : Fragment() {

    lateinit var binding: FragmentMainBinding

    private val viewModel by activityViewModels<MainViewModel>()
    private val transactionAdapter = TransactionAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvTransactions.apply {
            adapter = transactionAdapter
            layoutManager = LinearLayoutManager(requireActivity())
        }
        binding.btnAddTransaction.setOnClickListener {
            (requireActivity() as MainActivity).navigateToAddTransactionFragment()
        }
        viewModel.currencyResponse.observe(viewLifecycleOwner, {
            binding.tvCurrency.text = if (it.isFailure)
                getString(R.string.error)
            else
                getString(R.string.btc_to_usd_currency, it.getOrDefault(getString(R.string.error)))
        })
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.transactionFlow.collectLatest {
                transactionAdapter.submitData(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).setUpToolbar(false, R.string.app_name)
    }
}