package com.foxidron.cryptowallet.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.foxidron.cryptowallet.data.api.CurrencyApi
import com.foxidron.cryptowallet.data.db.TransactionsDao
import com.foxidron.cryptowallet.data.models.Transaction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel to make requests to currency api and show data in UI
 */

@HiltViewModel
class MainViewModel @Inject constructor(
    private val currencyApi: CurrencyApi,
    private val transactionsDao: TransactionsDao
) : ViewModel() {

    private var _currencyResponse = MutableLiveData<Result<String>>()
    val currencyResponse: LiveData<Result<String>>
        get() = _currencyResponse

    val transactionFlow = Pager(
        PagingConfig(pageSize = 20)
    ) {
        transactionsDao.getAllTransactions()
    }.flow.cachedIn(viewModelScope)

    fun updateCurrency() {
        viewModelScope.launch {
            try {
                val response = currencyApi.getCurrency()
                _currencyResponse.postValue(Result.success(response.bpi.USD.rate))
            } catch (e: Exception) {
                _currencyResponse.postValue(Result.failure(e))
            }
        }
    }

    fun insertTransaction(transaction: Transaction) {
        viewModelScope.launch {
            transactionsDao.insertTransaction(transaction)
        }
    }
}