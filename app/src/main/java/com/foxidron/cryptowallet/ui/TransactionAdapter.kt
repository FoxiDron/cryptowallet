package com.foxidron.cryptowallet.ui

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.foxidron.cryptowallet.data.models.Transaction
import com.foxidron.cryptowallet.databinding.TransactionItemBinding
import java.text.SimpleDateFormat
import java.util.*

/**
 * Adapter to host data
 */

class TransactionAdapter :
    PagingDataAdapter<Transaction, TransactionAdapter.TransactionViewHolder>(TransactionComparator) {

    private val printDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
    private val sameDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, if (position == 0) null else getItem(position - 1))
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransactionViewHolder {
        return TransactionViewHolder(
            TransactionItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    inner class TransactionViewHolder(private val binding: TransactionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(transaction: Transaction, prevItem: Transaction?) {
            binding.tvName.text = transaction.category
            binding.tvDate.text = printDateFormat.format(transaction.date)
            binding.tvAmount.text = transaction.amount.toString()
            binding.tvAmount.setTextColor(if (transaction.outlay) Color.RED else Color.GREEN)
            val headerDate = sameDateFormat.format(transaction.date)
            if (prevItem == null) {
                binding.tvDateSeparator.text = headerDate
                binding.tvDateSeparator.visibility = View.VISIBLE
            } else {
                if (headerDate == sameDateFormat.format(prevItem.date)) {
                    binding.tvDateSeparator.visibility = View.GONE
                } else {
                    binding.tvDateSeparator.text = headerDate
                    binding.tvDateSeparator.visibility = View.VISIBLE
                }
            }
        }
    }

    object TransactionComparator : DiffUtil.ItemCallback<Transaction>() {
        override fun areItemsTheSame(oldItem: Transaction, newItem: Transaction) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Transaction, newItem: Transaction) =
            oldItem == newItem
    }
}